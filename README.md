# Sinker

The classic 2D browser battleship game where players try to destroy ships of their opponents.

## Index
- [Screenshots](https://gitlab.com/jurajrob/sinker#screenshots)
- [Installation](https://gitlab.com/jurajrob/sinker#installation)
- [Controls](https://gitlab.com/jurajrob/sinker#controls)
- [How To Play](https://gitlab.com/jurajrob/sinker#how-to-play)
- [Documentation](https://gitlab.com/jurajrob/sinker#documentation)
    - [Code Structure](https://gitlab.com/jurajrob/sinker#code-structure)
        - [HTML](https://gitlab.com/jurajrob/sinker#html)
        - [CSS](https://gitlab.com/jurajrob/sinker#css)
        - [JavaScript](https://gitlab.com/jurajrob/sinker#javascript)
            - [ships.js](https://gitlab.com/jurajrob/sinker#shipsjs)
            - [person.js](https://gitlab.com/jurajrob/sinker#personjs)
            - [board.js](https://gitlab.com/jurajrob/sinker#boardjs)
            - [database.js](https://gitlab.com/jurajrob/sinker#databasejs)
            - [gameManager.js](https://gitlab.com/jurajrob/sinker#gamemanagerjs)
            - [sinker.js](https://gitlab.com/jurajrob/sinker#sinkerjs)
- [Further Development](https://gitlab.com/jurajrob/sinker#further-development)
- [Team](https://gitlab.com/jurajrob/sinker#team)
- [Contributors](https://gitlab.com/jurajrob/sinker#contributors)
- [Individual contributions](https://gitlab.com/jurajrob/sinker#individual-contributions)


## Screenshots

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

| ![Screen 1](https://i.imgur.com/JFScb2y.png) | ![Screen 2](https://i.imgur.com/gt4dYrw.png) |
|---------------------------------------------|---------------------------------------------|
| ![Screen 3](https://i.imgur.com/mAWE6PK.png)

## Installation

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)


1. :arrow_down: [For further development version](https://gitlab.com/jurajrob/sinker/-/archive/master/sinker-master.zip)
2. :arrow_down: Google Drive [Only to play version](https://drive.google.com/file/d/1akKZB7i_CVw0afOJjsm7p-GxarI_D8Vg/view?usp=sharing)
3. :arrow_down: Uloz.to [Only to play version](https://uloz.to/file/rDzG3dEvwt7N/only-to-play-zip
)
4. Press <kbd>Double Left Mouse Click</kbd> on `index.html` file to start the game in your browser.

## Controls

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

The game is played by <kbd>Mouse</kbd> and player names are entered by <kbd>keyboard</kbd>.

## How To Play

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

- Main Menu
    - <kbd>Singleplayer</kbd> - starts game against computer (AI)
    - <kbd>Multiplayer</kbd> - starts game against two players on the same computer
    - <kbd>Score Board</kbd> - shows table of match history  

- Singleplayer
    - you shoot on the board by clicking <kbd>Left Mouse Button</kbd>
    - when you hit an empty square it changes it's color to "gray"
    - when you hit the ship it changes it's color to "red"
    - shoot until you destroy all of the enemy ships
    - click <kbd>Main Menu</kbd> when you wish to end the game and return to the Main Menu  

- Multiplayer
    - at the start there is a popup message box where players can enter their names
    - if they don't, default names will be used
    - the rest is the same as in singleplayer mode  

- Score Board
    - a table that contains data about match history
    - data are updated only in multiplayer mode

## Documentation

### Code Structure

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

The code is divided into 3 main parts:  
1. HTML
2. CSS
3. JavaScript

![Code Structure](https://i.imgur.com/CJvceNw.png)  

### HTML

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

HTML contains only basic settings for web page and some crucial elements for better coding of javascript.

### CSS

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

Is to set up how individual elements of HTML should be displayed. There are settings for their position, size, color and more...    
For example, if you want to change the background color of the game, you can just change     `background-color` of `body` element.

### JavaScript

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

Backend of the game is coded in `javascript`, that is divided into multiple files by logic.
`main.js` is bundle of all javascript files. This bundle was created in order to run the game properly.  For more information read [Further Development](https://gitlab.com/jurajrob/sinker#further-development)

#### ships.js

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

This file contains class `Ship`, serving as a template for battleship objects. This class is extended by   child classes `Raft, Scout, Battleship, Arttilery`. Each of these child classes has its own parameters. If you want to create your own type of ship, you can just extend class `Ship` and set   your own parameters.

#### person.js

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

File includes class `Person`, that is a template for creating players. This class is extended by child    classes `Player` for human players and by class `AI`, that servs as representation of NPC. 
  
`AI` contains method `aiFire()` for shooting on player's board. When you want to modifiy Players   
or make better AI you can do it in this file.

#### board.js

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

Contains class `Grid` that represents one square of the game board. Theese are the most important   properties: `id` and `state`. `id` contains location of the grid and `state` stores information about, what the state of the current grid is.

The next class in this file is called `Board`. It discribes game board, its size, what ships are on   it, individual grids in 2D array and many more. Plus, it contains functionality to create a game   board, random layout of ships and what happens when someone shoots on the board.

- Method `createGrids()`, contains functionality for creating the whole game board.  
- `randomLoactions()`, is a method that describes how to randomly place ships on the board.  
- Method `fire()`, includes logic of what should happen when someone shoots at the board.  

#### database.js

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

File contains class `Database`, that includes methods for work with IndexedDB database.  

Be careful when you are working with it's functionalities, because there is asynchronous programming implemented.

#### gameManager.js

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

This file includes class `GameManager` and contains logic for controlling the flow of the whole game for the   
entire time from it's start.  
  
There are many methods, but some of the most important are:  
- `mainMenu()` - controls buttons in the main menu
- `singlePlayer()` - describes logic of singleplayer mode
- `multiPlayer()`-  describes logic of multiplayer mode
- `changeTurns()`-  to alternate turns between players
- `gameOver()` - what should happen when the game ends

When you would like to include some more functionality to how to control the flow of the game, you    should do it here. This class also contains Event Handlers that invoke methods after player actions    so you can modulate these here too.  

The code is made in a way so anybody can add their functionality quite easily, but be wary when you   are changing some built-in logic.

#### sinker.js

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

This file is the only one that is imported inside HTML document. It only contains one method `init()`,   that is called when the page is done loading. Inside the method is initialized `gameManager`   object and his mehthod `mainMenu()` is called.

## Further Development

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

In this project we used ECMAScript modules, thanks to which we could use `import` / `export` statments in our code. This modules can't be loaded from local files because of the CORS policy. In order to make our code    work properly, we bundled it into one `main.js` file. You can find an example how we imported our script into HTML document inside our index.html file.   
  
If you want to make som further development, you need to know how to bundle javascript files into one file. Here is a little help on how to do it:

1. You need to install Node.js [link](https://nodejs.org/en/download/).
2. When you downloaded our [`For further development`](https://gitlab.com/jurajrob/sinker/-/archive/master/sinker-master.zip) version, open `cmd` and navigate to your project folder.
3. Type command:

    ```bash
    npm run build
    ```
    `main.js` file should be created inside `dist` folder of your project.
4. In case there are some errors or unrecognized commands try insert this command first and then try to  build again:

    ```bash
    npm install --save-dev webpack-cli
    ```
**WARNING !!** Try not to change structure of already implemented import / export statements because, they need to be implemented in a way that is connected inside one file (in our case `sinker.js`), or the process of creating bundle will fail.

## Team

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

- Juraj Rób [@jurajrob](https://gitlab.com/jurajrob/) - Team leader
- Martin Brieška [@brieska.martin](https://gitlab.com/brieska.martin) - Programmer / Designer
- Stanislav Varga [@stanleyvarga](https://gitlab.com/stanleyvarga) - Programmer / Designer

## Contributors

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

- Sasa [@Sasa](https://s.team/p/pcd-wprn/dfqrkjhv)
    -  The Goddess of redemption in correcting English grammar.

## Individual contributions

[[Back to top]](https://gitlab.com/jurajrob/sinker#index)

- [@jurajrob](https://gitlab.com/jurajrob/)
    - I created most of the schedule in the MS Project environment. All team members participated in the   implementation design. I also created a project plan document by combining the data and informations we   created as a team. I defined the project objectives, expected results and outputs. I created index.html,    style.css and javascript files: board.js, database.js, gameManager.js, person.js, ships.js, sinker.js as well as this   README.md file.
- [@brieska.martin](https://gitlab.com/brieska.martin)
    - I mainly solved the technical part of exporting individual parts from MS Project such as tables   and graphs (Gantt, Org. Structure, timeline of milestones). I participated in naming the team and game. I also helped to solve the risks of the project and selecting the programming language. It was   my idea to create a bundle from javascript files.
- [@stanleyvarga](https://gitlab.com/stanleyvarga)
    - I chose the tools that will be used in the development of the project (development, communication, organizational). I helped choose the name of the game, the team and the   programming language. I defined the criteria for success of the project and helped with risk management. 




