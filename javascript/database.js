//
// this class contains functionality for working with IndexedDB
export default class Database {
    constructor() {
        this.version = 10; // increment if you need to call 'onupgradeneeded' funtion
    }

    // this method creates database if needed and destroy the previous one
    createDatabase() {
        return new Promise(resolve => {
            let request = window.indexedDB.open('players_db', this.version);

            request.onerror = function () {
                console.log('Database failed to open');
                resolve(false);
            };

            request.onsuccess = function (e) {

                console.log('Database opened successfully');
                resolve(true);
            }

            request.onupgradeneeded = function (e) {

                let db = request.result;


                // db.deleteObjectStore('players_db');

                db.createObjectStore('players_db', { keyPath: 'name' });

                console.log('Database setup complete');
                resolve(true);
            }



        });


    }

    // here you can add new players into database
    addData(data) {
        return new Promise(resolve => {

            let request = window.indexedDB.open('players_db', this.version);

            request.onerror = function () {
                console.log('Database failed to open');
                resolve(false);
            };

            request.onsuccess = function (e) {

                let db = request.result;

                data.forEach(player => {
                    db.transaction(['players_db'], 'readwrite').objectStore('players_db').add(player);
                });


                console.log('Database opened successfully');
                resolve(true);
            }


        });



    }

    // this method returns data of specific player
    getData(key) {

        return new Promise(resolve => {

            let request = window.indexedDB.open('players_db', this.version);

            request.onerror = function () {
                console.log('Database failed to open');
                resolve(false);
            };

            request.onsuccess = function (e) {

                let db = request.result;
                db.transaction('players_db').objectStore('players_db').get(key).onsuccess = function (e) {
                    console.log(e.target.result);
                    resolve(e.target.result);
                }

            }


        });


    }

    // updates data of currently playing players
    updateData(data) {

        return new Promise(resolve => {

            let request = window.indexedDB.open('players_db', this.version);

            request.onerror = function () {
                console.log('Database failed to open');
                resolve(false);
            };

            request.onsuccess = function (e) {

                let db = request.result;
                let objectStore = db.transaction(['players_db'], 'readwrite').objectStore('players_db');

                data.forEach(d => {
                    objectStore.put(d);
                });


                resolve(true);
            }
        });



    }

    // diplaying database as table on the screen
    async displayData() {

        return new Promise(resolve => {
            let request = window.indexedDB.open('players_db', this.version);

            request.onerror = function () {
                console.log('Database failed to open');
                alert('Database is Empty');

                resolve(false);
            };

            request.onsuccess = function (e) {
                let table = document.getElementById('player-data');
                while (table.firstChild) {
                    table.removeChild(table.lastChild);
                }


                let db = request.result;
                let objectStore = db.transaction('players_db').objectStore('players_db');
                objectStore.openCursor().onsuccess = function (e) {

                    let cursor = e.target.result;


                    if (cursor) {
                        let tr = document.createElement('tr');
                        let td = [];

                        for (let i = 0; i < 4; i++) {
                            td[i] = document.createElement('td');
                            tr.appendChild(td[i]);
                        }

                        td[0].textContent = cursor.value.name;
                        td[1].textContent = cursor.value.score;
                        td[2].textContent = cursor.value.games;
                        td[3].textContent = cursor.value.wins;

                        table.appendChild(tr);

                        cursor.continue();
                    }

                    document.getElementById('wrapper').hidden = true;
                    document.getElementById('tbl-db').hidden = false;
                }
                console.log('Database opened successfully');

                resolve(true);
            }
            console.log('displayed');

        });
    }



}