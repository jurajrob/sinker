import { Raft, Scout, Battleship, Artillery } from './ships.js'

// possible states of grid
export const STATES = {
    EMPTY: 'empty',
    OCCUPIED: 'occupied',
    DESTROYED: 'destroyed',
    MISSED: 'missed'
}

// grid represents one box on game board
// size = size of displayed grid in pixels; state = state of current grid; 
// id = position on board 
class Grid {
    constructor() {
        this.size = 50;
        this.state = STATES.EMPTY;
        this.id;
    }
}

//repressents game board
export class Board {
    constructor(gameBoardElement, nameRowsElement, nameColsElement, idMark) {

        this.size = 10;
        this.grids = new Array(this.size); // contains 10x10 grids
        this.nameRows = new Array(this.size); // contains grids for naming rows

        // ships placed on board
        this.ships = [new Raft(), new Raft(), new Raft(), new Scout(), new Battleship(), new Battleship(), new Artillery()];
        //
        //for testings
        //this.ships = [new Raft()];

        // creating 2D array
        for (let i = 0; i < this.size; i++) {
            this.grids[i] = new Array(this.size);
            for (let j = 0; j < this.size; j++) {
                this.grids[i][j] = new Grid();
            }
            this.nameRows[i] = new Grid();
        }


        this.gameBoardElement = document.getElementById(gameBoardElement);
        this.nameRowsElement = document.getElementById(nameRowsElement);
        this.nameColsElement = document.getElementById(nameColsElement);

        // idMark marks grids locations for specific game board
        this.idMark = idMark;
        // if its possible to click on board
        this.active = false;
        // if all ships on board where destroyed(sunken)
        this.endGame = false;

        this.createGrids().randomLocations();

    }

    //
    // method creates game board by individual grids, that are placed in the HTML div element
    // and local 2d array grids
    createGrids() {
        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {

                // adding new grid to the HTML
                let newGrid = document.createElement('div');
                this.gameBoardElement.appendChild(newGrid);

                // assign location as ID to div element and into local grids array
                newGrid.id = this.idMark + j + i;
                this.grids[i][j].id = this.idMark + j + i;

                // callculate position for current new grid
                let topPosition = j * this.grids[i][j].size;
                let leftPosition = i * this.grids[i][j].size;

                // positioning new grid
                newGrid.style.top = (topPosition + newGrid.parentElement.offsetTop) + 'px';
                newGrid.style.left = (leftPosition + newGrid.parentElement.offsetLeft) + 'px';
            }
        }

        //this.createNameRows();
        // this.createNameCols();

        return this;
    };

    // creates elements for naming rows 'UNCOMPLETE!!'
    createNameRows() {
        console.log(this.nameRowsElement);

        let aplhabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

        for (let i = 0; i < this.size; i++) {
            let newGrid = document.createElement('div');
            this.nameRowsElement.appendChild(newGrid);

            let newText = document.createElement('p');
            newText.id = 'name-row-p';
            newText.textContent = aplhabet[i];


            newGrid.appendChild(newText);

            let topPosition = i * this.nameRows[i].size;

            newGrid.style.top = (topPosition + newGrid.parentElement.offsetTop) + 'px';
            //newGrid.style.left = newGrid.parentElement.offsetLeft + 'px';

        }
    }

    // not working correctly
    createNameCols() {
        console.log(this.nameColsElement);

        for (let i = 0; i < this.size; i++) {
            let newGrid = document.createElement('div');
            this.nameColsElement.appendChild(newGrid);

            let newText = document.createElement('p');
            newText.id = 'name-col-p';
            newText.textContent = i;


            newGrid.appendChild(newText);

            let leftPosition = i * this.nameRows[i].size;

            newGrid.style.top = 100 + 'px';
            //newGrid.style.left = newGrid.parentElement.offsetLeft + 'px';

        }

    }

    //
    // this method distribute ships across the board randomly
    // at first is randomly generated direction of the ship, according to that is called proper method
    randomLocations() {

        for (let i = 0; i < this.ships.length; i++) {

            // 0 = up, 1 = right direction of ship
            let direction = Math.floor(Math.random() * 2);

            switch (direction) {
                case 0:
                    this.randomLocationsBottom(this.ships[i], direction);
                    break;

                default:
                    this.randomLocationsRight(this.ships[i], direction);
                    break;
            }
        }

    };

    //
    // post ship at random positions in direction from TOP to BOTTOM
    randomLocationsBottom(ship, dir) {
        while (true) {
            let x = Math.floor(Math.random() * 10);
            let y = Math.floor(Math.random() * 10);

            // checking if the ship will fill inside the board;
            // if it does, the next is collision detection  to find out if grids in the direction are 'EMPTY';
            // in case that the ship doesn`t fill iside the board or there`s collision detected
            // the next iteration of the loop is started, and new coordinates are generated
            if ((x + ship.size) <= this.size) {

                if (this.checkCollision(x, y, ship.size, dir))
                    continue;
            }
            else
                continue;

            // changing state of grid on 'OCCUPIED' and storing coordinates inside the ship
            for (let j = 0; j < ship.size; j++) {
                this.grids[x + j][y].state = STATES.OCCUPIED;
                ship.locations.push(this.idMark + (x + j) + y);

                // showing new locations on board for testing
                //document.getElementById(this.idMark + (x + j) + y).style.backgroundColor = 'blue';
            }
            return;
        }
    };

    //
    // same as previous method, it`just in the direction from LEFT to RIGHT
    randomLocationsRight(ship, dir) {
        while (true) {
            let x = Math.floor(Math.random() * 10);
            let y = Math.floor(Math.random() * 10);

            if ((y + ship.size) <= this.size) {
                if (this.checkCollision(x, y, ship.size, dir))
                    continue;
            }
            else
                continue;

            for (let j = 0; j < ship.size; j++) {
                this.grids[x][y + j].state = STATES.OCCUPIED;
                ship.locations.push(this.idMark + x + (y + j));

                // showing new locations on board for testing
                //document.getElementById(this.idMark + x + (y + j)).style.backgroundColor = 'blue';
            }
            return;
        }
    };

    //
    //checikng collision, 
    //if the grids in the direction of the newly placed ship are OCCUPIED
    checkCollision(row, col, shipSize, dir) {
        switch (dir) {
            case 0:

                for (let i = row; i < (row + shipSize); i++) {
                    if (this.grids[i][col].state === STATES.OCCUPIED)
                        return true;
                }
                return false;

            default:
                for (let i = col; i < (col + shipSize); i++) {
                    if (this.grids[row][i].state === STATES.OCCUPIED)
                        return true;
                }
                return false;
        }
    };

    //
    // Event Handler for Left Mouse click on the grid
    // e.currentTarget is board div element, but we want grid, that is inside e.target
    fireOnMouseClick(e) {

        let result;
        if (e.target !== e.currentTarget) {

            result = this.fire(e.target.id);

            //alert('Clicked on row ' + row + ', col ' + col);
        }
        // this is to stop bubbling
        e.stopPropagation();
        return result;
    };

    //
    // this method contains whole logic of shooting on the grids
    fire(location) {

        // parssing 'row' and 'col' from string coordinates
        let row = location.substring(2, 3);
        let col = location.substring(3, 4);
        let target = document.getElementById(location);


        // if the target gird is 'EMPTY', player 'MISSED'
        // 'return values' are for AI, so it could determine if it hits
        // new target or previously hit target
        if (this.grids[row][col].state === STATES.EMPTY) {
            target.style.backgroundColor = '#bbb';
            this.grids[row][col].state = STATES.MISSED;

            return true;
            // else if grid is 'OCCUPIED', player hit the ship
        } else if (this.grids[row][col].state === STATES.OCCUPIED) {

            target.style.backgroundColor = 'red';
            this.grids[row][col].state = STATES.DESTROYED;

            // getting the currently hit ship, and increment number of hits
            let targetShip = this.ships.findIndex(f => f.locations.includes(location));
            this.ships[targetShip].hits++;

            // if number of hits is the same as size of the ship, the ship is sunken
            if (this.ships[targetShip].hits === this.ships[targetShip].size)
                this.ships[targetShip].sunken = true;

            // checking if game ended
            this.endGame = this.checkEndGame();

            return true;

            // checking if player fired on previously clicked grid
        } else if (this.grids[row][col].state === STATES.DESTROYED || this.grids[row][col].state === STATES.MISSED) {
            //alert('You already fired at this location.');
            return false;
        }
    };
    //
    // if all ships are sunked, game ends
    checkEndGame() {

        for (let i = 0; i < this.ships.length; i++) {
            if (!this.ships[i].sunken) {
                return false;
            }
        }
        return true;
    };
}