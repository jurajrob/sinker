import { Player, AI } from './person.js';
import Database from './database.js';
import { STATES } from './board.js'

//
// this class manages flow of the game
export default class GameManager {
    constructor() {

        this.player1;
        this.player2;
        this.database = new Database();
        // if the first turn was already played
        this.firstTurn = true;
        // if its singleplayer or multiplayer mode
        this.single;

        // display settings for player`s Turns
        this.turnNoticeText = document.getElementById('turn');
        this.hidden = 0.3;
        this.visible = 1;
    }

    // represents main menu functionality
    mainMenu() {

        let singleplayerBtn = document.getElementById('btn-singleplayer');
        let multiplayerBtn = document.getElementById('btn-multiplayer');
        let scoreBoardBtn = document.getElementById('btn-score-board');

        //
        //single player event handler
        let spEventHandler = function () {
            this.singlePlayer(this);
            singleplayerBtn.hidden = true;
            multiplayerBtn.hidden = true;
            scoreBoardBtn.hidden = true;
        }.bind(this);

        //
        // start game button Event handler
        let mpEventHandler = function () {
            this.multiPlayer(this);
            singleplayerBtn.hidden = true;
            multiplayerBtn.hidden = true;
            scoreBoardBtn.hidden = true;
        }.bind(this);

        //
        // score board button Event handler
        let scEventHandler = function () { if (this.database.displayData()) return; }.bind(this);

        singleplayerBtn.addEventListener('click', spEventHandler);
        multiplayerBtn.addEventListener('click', mpEventHandler);
        scoreBoardBtn.addEventListener('click', scEventHandler);
    }

    readyHTMLElementsForGame() {
        document.getElementById('tbl-db').hidden = true;
        document.getElementById('wrapper').hidden = false;
        let mainMenuBtn = document.getElementById('btn-main-menu');
        mainMenuBtn.hidden = false;
        mainMenuBtn.addEventListener('click', this.reload);

    }

    //
    // functionality for singleplayer game mode
    singlePlayer(self) {

        this.single = true;
        //
        // ready HTMl elements
        this.readyHTMLElementsForGame();

        //
        // create objects of Players
        this.player1 = new Player('board1', 'name-row1', 'name-col1', 'b1');
        this.player2 = new AI('board2', 'name-row2', 'name-col2', 'b2');

        this.player2.name = 'AI';

        //
        // display names on top game boards
        this.insertNames();

        //
        // evenhandler for shooting
        const eventHandler = function (e) { if (!this.active) return; if (this.fireOnMouseClick(e)) { self.changeTurns(); }; };

        this.player2.board.gameBoardElement.addEventListener("click", eventHandler.bind(this.player2.board), false);

        this.changeTurns();
    }

    //
    // functionality for multiplayer game mode
    async multiPlayer(self) {

        this.single = false;
        //
        // ready HTMl elements
        this.readyHTMLElementsForGame();

        //
        // create objects of Players
        this.player1 = new Player('board1', 'name-row1', 'name-col1', 'b1');
        this.player2 = new Player('board2', 'name-row2', 'name-col2', 'b2');

        //
        // get Players names from users
        this.insertNames();


        // insert players inside database if they are new
        await this.database.addData(this.prepareData());

        //
        // get data of Players from database
        let player1Data = await this.database.getData(this.player1.name);
        let player2Data = await this.database.getData(this.player2.name);

        //
        // assing data from database
        this.player1.updateData(player1Data);
        this.player2.updateData(player2Data);


        console.log(this.player1);
        console.log(this.player2);


        //
        // evenhandler for shooting
        const eventHandler = function (e) { if (!this.active) return; if (this.fireOnMouseClick(e)) { self.changeTurns(); }; };

        // adding Event Listener on "board" div element so we can call Event Handler,
        // but if we want to store a refference of board object so we could access it`s propeties inside,
        // we have to "bind" it!!
        this.player1.board.gameBoardElement.addEventListener("click", eventHandler.bind(this.player1.board), false);

        this.player2.board.gameBoardElement.addEventListener("click", eventHandler.bind(this.player2.board), false);

        this.changeTurns();
    }

    //
    // inserting names from prompt boxes and displaying them
    // over the top of game boards
    insertNames() {
        if (!this.single) {
            let name1 = prompt('Enter Player 1`s name', 'Player 1');
            if (name1 === null || name1 === "") {
                name1 = 'Player 1';
            }
            this.player1.name = name1;

            let name2 = prompt('Enter Player 2`s name', 'Player 2');
            if (name2 === null || name2 === "") {
                name2 = 'Player 2';
            }

            this.player2.name = name2;
        }

        document.getElementById('p1').textContent = this.player1.name;
        document.getElementById('p2').textContent = this.player2.name;

    }

    //
    //settings for player1`s turn
    player1Turn() {

        this.player1.board.active = false;
        // player1 can click on player2`s board
        this.player2.board.active = true;

        this.turnNoticeText.textContent = `TURN: ${this.player1.name}`;

        this.player1.board.gameBoardElement.style.opacity = this.hidden;
        this.player2.board.gameBoardElement.style.opacity = this.visible;
    }

    //
    //settings for player2`s turn
    player2Turn() {
        this.player2.board.active = false;
        // player2 can click on player1`s board
        this.player1.board.active = true;

        this.turnNoticeText.textContent = `TURN: ${this.player2.name}`;

        this.player2.board.gameBoardElement.style.opacity = this.hidden;
        this.player1.board.gameBoardElement.style.opacity = this.visible;

    }

    //
    // functionality for changing turns
    changeTurns() {

        // deciding the first turn
        if (this.firstTurn) {

            this.firstTurn = false;

            let result = Math.floor(Math.random() * 2);

            // '0' means its player1 turn, but it will swapped in lower code
            if (result == 0) {
                this.player2.board.active = true;
            } else {
                this.player1.board.active = true;
            }
        }

        // checking game over
        if (this.player1.board.endGame || this.player2.board.endGame) {
            this.gameOver();
        } else {

            // swap players turns
            switch (this.player1.board.active) {
                case true:
                    this.player1Turn();
                    break;

                default:
                    this.player2Turn();

                    // functionality for single player
                    if (this.single) {
                        let enemyTarget = this.player1;
                        let prepare = function () { this.aiFire(enemyTarget); };
                        let aiShoots = prepare.bind(this.player2);
                        aiShoots();
                        this.changeTurns();
                    }
                    break;
            }
        }
    }

    //
    // prepare players`s data for insertion into database
    prepareData() {
        let data = [
            { name: this.player1.name, score: this.player1.score, games: this.player1.games, wins: this.player1.wins },
            { name: this.player2.name, score: this.player2.score, games: this.player2.games, wins: this.player2.wins },
        ]

        return data;
    }


    //
    // reloads page and returns into main menu
    // effective only for offline game
    reload() {
        location.reload();
        return false;
    }

    //
    // resets board`s settings and data for replay
    resetBoard(brd) {

        brd.endGame = false;

        while (brd.gameBoardElement.firstChild) {
            brd.gameBoardElement.removeChild(brd.gameBoardElement.lastChild);
        }

        for (let i = 0; i < brd.size; i++) {
            for (let j = 0; j < brd.size; j++) {
                brd.grids[i][j].state = STATES.EMPTY;

            }
        }

        brd.ships.forEach(ship => {
            ship.hits = 0;
            ship.sunken = false;
        });
    }

    //
    // resets settings and starting replay
    replay() {
        this.player1.score = 100;
        this.player1.score = 100;

        this.resetBoard(this.player1.board);
        this.resetBoard(this.player2.board);

        this.player1.board.createGrids().randomLocations();
        this.player2.board.createGrids().randomLocations();

        this.firstTurn();

        console.log(this.player1);
        console.log(this.player2);
    }

    //
    // calculate score points
    calculatePoints(array) {
        let counter = 0;
        let points = 100;

        console.log(array);

        for (let i = 0; i < array.length; i++) {
            for (let j = 0; j < array.length; j++) {
                if (array[i][j].state === STATES.MISSED) {
                    counter++;
                    console.log(counter + '' + array[i][j].id);
                }
            }
        }

        return (points - counter);
    }

    //
    // gameover functionality
    async gameOver() {
        this.player1.games++;
        this.player2.games++;

        if (this.player1.board.endGame) {
            this.player2.wins++;
            this.player2.score += this.calculatePoints(this.player1.board.grids);
            this.player1.score = 0;

        } else {
            this.player1.wins++;
            this.player1.score += this.calculatePoints(this.player2.board.grids);
            this.player2.score = 0;
        }

        console.log(this.player1);
        console.log(this.player2);

        // update data in database
        await this.database.updateData(this.prepareData(this.player1));
        await this.database.updateData(this.prepareData(this.player2));

        // ask players for replay
        let result = confirm(`Game Over: \n ${this.player1.board.endGame ? this.player2.name : this.player1.name} wins!!
        Replay?`);
        if (result) {
            this.replay();

        } else {
            this.reload();
        }
    }
}
