import GameManager from './gameManager.js'

//
// function called when page is loaded
// intializing gameManager and starting Main Menu
async function init() {
    let gameManager = new GameManager();
    // open / set up database
    await gameManager.database.createDatabase();
    let mainMenu = gameManager.mainMenu.bind(gameManager);
    mainMenu();
}

// call funciton 'init' when page is loaded
window.onload = init;
