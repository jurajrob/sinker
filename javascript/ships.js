//
// template for Ship,
class Ship {
    constructor() {
        this.name;
        this.size;
        this.locations = [];
        this.hits = 0;
        this.sunken = false;
    }
}

//
// Inheritance:
// Raft is type of ship whit its own "name" and "size"
export class Raft extends Ship {
    constructor() {
        super();
        super.name = 'raft';
        super.size = 2;
    }
}

export class Scout extends Ship {
    constructor() {
        super();
        super.name = 'scout';
        super.size = 3;
    }
}

export class Battleship extends Ship {
    constructor() {
        super();
        super.name = 'battleship';
        super.size = 4;
    }
}

export class Artillery extends Ship {
    constructor() {
        super();
        super.name = 'artillery';
        super.size = 5;
    }
}




