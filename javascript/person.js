import { Board, STATES } from "./board.js";
//
// template for person
class Person {
    constructor() {
        this.name;
        this.score;
        this.games;
        this.wins;

    }
}

//
// player representation
export class Player extends Person {
    constructor(gameBoardElement, nameRowsElement, nameColsElement, idMark) {
        super();
        this.name = 'Player';
        this.score = 0;
        this.games = 0;
        this.wins = 0;
        this.board = new Board(gameBoardElement, nameRowsElement, nameColsElement, idMark);
    }

    // update data when loaded from database
    updateData(data) {

        this.score = data.score;
        this.games = data.games;
        this.wins = data.wins;

    };
}

//
// AI representation
export class AI extends Person {

    constructor(gameBoardElement, nameRowsElement, nameColsElement, idMark) {
        super();
        this.name = 'Player';
        this.score = 0;
        this.games = 0;
        this.wins = 0;
        this.board = new Board(gameBoardElement, nameRowsElement, nameColsElement, idMark);
        this.nextTarget = []; // array of next target after succesful hit
    }

    //
    // method for shooting
    aiFire(player) {
        let range = player.board.size;
        let id = player.board.idMark;
        let row, col;
        let oldTargetHit; // result if target was already hit in the past
        let target; // location of target to be hit

        // repead until hit correct target
        while (1) {

            if (this.nextTarget.length > 0) {
                let next = this.nextTarget.pop();

                row = next.substring(2, 3);
                col = next.substring(3, 4);
            }
            else {
                row = Math.floor(Math.random() * range).toString();
                col = Math.floor(Math.random() * range).toString();

            }

            target = id + row + col;

            //
            // if the target is ship, set next target around it
            if (player.board.grids[row][col].state === STATES.OCCUPIED) {
                this.setNextTarget(id, row, col, range);
            }

            let prepare = function func() {

                oldTargetHit = this.fire(target);
            }
            let shoot = prepare.bind(player.board);
            shoot();

            if (oldTargetHit) {
                break;
            }

        }

    }

    //
    // setting next targets
    setNextTarget(id, row, col, range) {

        // locations are saved as string
        row = Number(row);
        col = Number(col);

        // next target up
        if ((row - 1) >= 0) {
            this.nextTarget.push(id + (row - 1) + col);
        }
        // next target down
        if ((row + 1) < range) {
            this.nextTarget.push(id + (row + 1) + col);

        }
        // next target left
        if ((col - 1) >= 0) {
            this.nextTarget.push(id + row + (col - 1));
        }
        // next target right
        if ((col + 1) < range) {
            this.nextTarget.push(id + row + (col + 1));
        }
    }


}

