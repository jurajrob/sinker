const path = require('path');

module.exports = {
    mode: 'none',
    entry: './javascript/sinker.js',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist'),
    },
};