/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _gameManager_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);


//
// function called when page is loaded
// intializing gameManager and starting Main Menu
async function init() {
    let gameManager = new _gameManager_js__WEBPACK_IMPORTED_MODULE_0__["default"]();
    // open / set up database
    await gameManager.database.createDatabase();
    let mainMenu = gameManager.mainMenu.bind(gameManager);
    mainMenu();
}

// call funciton 'init' when page is loaded
window.onload = init;


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return GameManager; });
/* harmony import */ var _person_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var _database_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5);
/* harmony import */ var _board_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);




//
// this class manages flow of the game
class GameManager {
    constructor() {

        this.player1;
        this.player2;
        this.database = new _database_js__WEBPACK_IMPORTED_MODULE_1__["default"]();
        // if the first turn was already played
        this.firstTurn = true;
        // if its singleplayer or multiplayer mode
        this.single;

        // display settings for player`s Turns
        this.turnNoticeText = document.getElementById('turn');
        this.hidden = 0.3;
        this.visible = 1;
    }

    // represents main menu functionality
    mainMenu() {

        let singleplayerBtn = document.getElementById('btn-singleplayer');
        let multiplayerBtn = document.getElementById('btn-multiplayer');
        let scoreBoardBtn = document.getElementById('btn-score-board');

        //
        //single player event handler
        let spEventHandler = function () {
            this.singlePlayer(this);
            singleplayerBtn.hidden = true;
            multiplayerBtn.hidden = true;
            scoreBoardBtn.hidden = true;
        }.bind(this);

        //
        // start game button Event handler
        let mpEventHandler = function () {
            this.multiPlayer(this);
            singleplayerBtn.hidden = true;
            multiplayerBtn.hidden = true;
            scoreBoardBtn.hidden = true;
        }.bind(this);

        //
        // score board button Event handler
        let scEventHandler = function () { if (this.database.displayData()) return; }.bind(this);

        singleplayerBtn.addEventListener('click', spEventHandler);
        multiplayerBtn.addEventListener('click', mpEventHandler);
        scoreBoardBtn.addEventListener('click', scEventHandler);
    }

    readyHTMLElementsForGame() {
        document.getElementById('tbl-db').hidden = true;
        document.getElementById('wrapper').hidden = false;
        let mainMenuBtn = document.getElementById('btn-main-menu');
        mainMenuBtn.hidden = false;
        mainMenuBtn.addEventListener('click', this.reload);

    }

    //
    // functionality for singleplayer game mode
    singlePlayer(self) {

        this.single = true;
        //
        // ready HTMl elements
        this.readyHTMLElementsForGame();

        //
        // create objects of Players
        this.player1 = new _person_js__WEBPACK_IMPORTED_MODULE_0__["Player"]('board1', 'name-row1', 'name-col1', 'b1');
        this.player2 = new _person_js__WEBPACK_IMPORTED_MODULE_0__["AI"]('board2', 'name-row2', 'name-col2', 'b2');

        this.player2.name = 'AI';

        //
        // display names on top game boards
        this.insertNames();

        //
        // evenhandler for shooting
        const eventHandler = function (e) { if (!this.active) return; if (this.fireOnMouseClick(e)) { self.changeTurns(); }; };

        this.player2.board.gameBoardElement.addEventListener("click", eventHandler.bind(this.player2.board), false);

        this.changeTurns();
    }

    //
    // functionality for multiplayer game mode
    async multiPlayer(self) {

        this.single = false;
        //
        // ready HTMl elements
        this.readyHTMLElementsForGame();

        //
        // create objects of Players
        this.player1 = new _person_js__WEBPACK_IMPORTED_MODULE_0__["Player"]('board1', 'name-row1', 'name-col1', 'b1');
        this.player2 = new _person_js__WEBPACK_IMPORTED_MODULE_0__["Player"]('board2', 'name-row2', 'name-col2', 'b2');

        //
        // get Players names from users
        this.insertNames();


        // insert players inside database if they are new
        await this.database.addData(this.prepareData());

        //
        // get data of Players from database
        let player1Data = await this.database.getData(this.player1.name);
        let player2Data = await this.database.getData(this.player2.name);

        //
        // assing data from database
        this.player1.updateData(player1Data);
        this.player2.updateData(player2Data);


        console.log(this.player1);
        console.log(this.player2);


        //
        // evenhandler for shooting
        const eventHandler = function (e) { if (!this.active) return; if (this.fireOnMouseClick(e)) { self.changeTurns(); }; };

        // adding Event Listener on "board" div element so we can call Event Handler,
        // but if we want to store a refference of board object so we could access it`s propeties inside,
        // we have to "bind" it!!
        this.player1.board.gameBoardElement.addEventListener("click", eventHandler.bind(this.player1.board), false);

        this.player2.board.gameBoardElement.addEventListener("click", eventHandler.bind(this.player2.board), false);

        this.changeTurns();
    }

    //
    // inserting names from prompt boxes and displaying them
    // over the top of game boards
    insertNames() {
        if (!this.single) {
            let name1 = prompt('Enter Player 1`s name', 'Player 1');
            if (name1 === null || name1 === "") {
                name1 = 'Player 1';
            }
            this.player1.name = name1;

            let name2 = prompt('Enter Player 2`s name', 'Player 2');
            if (name2 === null || name2 === "") {
                name2 = 'Player 2';
            }

            this.player2.name = name2;
        }

        document.getElementById('p1').textContent = this.player1.name;
        document.getElementById('p2').textContent = this.player2.name;

    }

    //
    //settings for player1`s turn
    player1Turn() {

        this.player1.board.active = false;
        // player1 can click on player2`s board
        this.player2.board.active = true;

        this.turnNoticeText.textContent = `TURN: ${this.player1.name}`;

        this.player1.board.gameBoardElement.style.opacity = this.hidden;
        this.player2.board.gameBoardElement.style.opacity = this.visible;
    }

    //
    //settings for player2`s turn
    player2Turn() {
        this.player2.board.active = false;
        // player2 can click on player1`s board
        this.player1.board.active = true;

        this.turnNoticeText.textContent = `TURN: ${this.player2.name}`;

        this.player2.board.gameBoardElement.style.opacity = this.hidden;
        this.player1.board.gameBoardElement.style.opacity = this.visible;

    }

    //
    // functionality for changing turns
    changeTurns() {

        // deciding the first turn
        if (this.firstTurn) {

            this.firstTurn = false;

            let result = Math.floor(Math.random() * 2);

            // '0' means its player1 turn, but it will swapped in lower code
            if (result == 0) {
                this.player2.board.active = true;
            } else {
                this.player1.board.active = true;
            }
        }

        // checking game over
        if (this.player1.board.endGame || this.player2.board.endGame) {
            this.gameOver();
        } else {

            // swap players turns
            switch (this.player1.board.active) {
                case true:
                    this.player1Turn();
                    break;

                default:
                    this.player2Turn();

                    // functionality for single player
                    if (this.single) {
                        let enemyTarget = this.player1;
                        let prepare = function () { this.aiFire(enemyTarget); };
                        let aiShoots = prepare.bind(this.player2);
                        aiShoots();
                        this.changeTurns();
                    }
                    break;
            }
        }
    }

    //
    // prepare players`s data for insertion into database
    prepareData() {
        let data = [
            { name: this.player1.name, score: this.player1.score, games: this.player1.games, wins: this.player1.wins },
            { name: this.player2.name, score: this.player2.score, games: this.player2.games, wins: this.player2.wins },
        ]

        return data;
    }


    //
    // reloads page and returns into main menu
    // effective only for offline game
    reload() {
        location.reload();
        return false;
    }

    //
    // resets board`s settings and data for replay
    resetBoard(brd) {

        brd.endGame = false;

        while (brd.gameBoardElement.firstChild) {
            brd.gameBoardElement.removeChild(brd.gameBoardElement.lastChild);
        }

        for (let i = 0; i < brd.size; i++) {
            for (let j = 0; j < brd.size; j++) {
                brd.grids[i][j].state = _board_js__WEBPACK_IMPORTED_MODULE_2__["STATES"].EMPTY;

            }
        }

        brd.ships.forEach(ship => {
            ship.hits = 0;
            ship.sunken = false;
        });
    }

    //
    // resets settings and starting replay
    replay() {
        this.player1.score = 100;
        this.player1.score = 100;

        this.resetBoard(this.player1.board);
        this.resetBoard(this.player2.board);

        this.player1.board.createGrids().randomLocations();
        this.player2.board.createGrids().randomLocations();

        this.firstTurn();

        console.log(this.player1);
        console.log(this.player2);
    }

    //
    // calculate score points
    calculatePoints(array) {
        let counter = 0;
        let points = 100;

        console.log(array);

        for (let i = 0; i < array.length; i++) {
            for (let j = 0; j < array.length; j++) {
                if (array[i][j].state === _board_js__WEBPACK_IMPORTED_MODULE_2__["STATES"].MISSED) {
                    counter++;
                    console.log(counter + '' + array[i][j].id);
                }
            }
        }

        return (points - counter);
    }

    //
    // gameover functionality
    async gameOver() {
        this.player1.games++;
        this.player2.games++;

        if (this.player1.board.endGame) {
            this.player2.wins++;
            this.player2.score += this.calculatePoints(this.player1.board.grids);
            this.player1.score = 0;

        } else {
            this.player1.wins++;
            this.player1.score += this.calculatePoints(this.player2.board.grids);
            this.player2.score = 0;
        }

        console.log(this.player1);
        console.log(this.player2);

        // update data in database
        await this.database.updateData(this.prepareData(this.player1));
        await this.database.updateData(this.prepareData(this.player2));

        // ask players for replay
        let result = confirm(`Game Over: \n ${this.player1.board.endGame ? this.player2.name : this.player1.name} wins!!
        Replay?`);
        if (result) {
            this.replay();

        } else {
            this.reload();
        }
    }
}


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Player", function() { return Player; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AI", function() { return AI; });
/* harmony import */ var _board_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);

//
// template for person
class Person {
    constructor() {
        this.name;
        this.score;
        this.games;
        this.wins;

    }
}

//
// player representation
class Player extends Person {
    constructor(gameBoardElement, nameRowsElement, nameColsElement, idMark) {
        super();
        this.name = 'Player';
        this.score = 0;
        this.games = 0;
        this.wins = 0;
        this.board = new _board_js__WEBPACK_IMPORTED_MODULE_0__["Board"](gameBoardElement, nameRowsElement, nameColsElement, idMark);
    }

    // update data when loaded from database
    updateData(data) {

        this.score = data.score;
        this.games = data.games;
        this.wins = data.wins;

    };
}

//
// AI representation
class AI extends Person {

    constructor(gameBoardElement, nameRowsElement, nameColsElement, idMark) {
        super();
        this.name = 'Player';
        this.score = 0;
        this.games = 0;
        this.wins = 0;
        this.board = new _board_js__WEBPACK_IMPORTED_MODULE_0__["Board"](gameBoardElement, nameRowsElement, nameColsElement, idMark);
        this.nextTarget = []; // array of next target after succesful hit
    }

    //
    // method for shooting
    aiFire(player) {
        let range = player.board.size;
        let id = player.board.idMark;
        let row, col;
        let oldTargetHit; // result if target was already hit in the past
        let target; // location of target to be hit

        // repead until hit correct target
        while (1) {

            if (this.nextTarget.length > 0) {
                let next = this.nextTarget.pop();

                row = next.substring(2, 3);
                col = next.substring(3, 4);
            }
            else {
                row = Math.floor(Math.random() * range).toString();
                col = Math.floor(Math.random() * range).toString();

            }

            target = id + row + col;

            //
            // if the target is ship, set next target around it
            if (player.board.grids[row][col].state === _board_js__WEBPACK_IMPORTED_MODULE_0__["STATES"].OCCUPIED) {
                this.setNextTarget(id, row, col, range);
            }

            let prepare = function func() {

                oldTargetHit = this.fire(target);
            }
            let shoot = prepare.bind(player.board);
            shoot();

            if (oldTargetHit) {
                break;
            }

        }

    }

    //
    // setting next targets
    setNextTarget(id, row, col, range) {

        // locations are saved as string
        row = Number(row);
        col = Number(col);

        // next target up
        if ((row - 1) >= 0) {
            this.nextTarget.push(id + (row - 1) + col);
        }
        // next target down
        if ((row + 1) < range) {
            this.nextTarget.push(id + (row + 1) + col);

        }
        // next target left
        if ((col - 1) >= 0) {
            this.nextTarget.push(id + row + (col - 1));
        }
        // next target right
        if ((col + 1) < range) {
            this.nextTarget.push(id + row + (col + 1));
        }
    }


}



/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STATES", function() { return STATES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Board", function() { return Board; });
/* harmony import */ var _ships_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4);


// possible states of grid
const STATES = {
    EMPTY: 'empty',
    OCCUPIED: 'occupied',
    DESTROYED: 'destroyed',
    MISSED: 'missed'
}

// grid represents one box on game board
// size = size of displayed grid in pixels; state = state of current grid; 
// id = position on board 
class Grid {
    constructor() {
        this.size = 50;
        this.state = STATES.EMPTY;
        this.id;
    }
}

//repressents game board
class Board {
    constructor(gameBoardElement, nameRowsElement, nameColsElement, idMark) {

        this.size = 10;
        this.grids = new Array(this.size); // contains 10x10 grids
        this.nameRows = new Array(this.size); // contains grids for naming rows

        // ships placed on board
        this.ships = [new _ships_js__WEBPACK_IMPORTED_MODULE_0__["Raft"](), new _ships_js__WEBPACK_IMPORTED_MODULE_0__["Raft"](), new _ships_js__WEBPACK_IMPORTED_MODULE_0__["Raft"](), new _ships_js__WEBPACK_IMPORTED_MODULE_0__["Scout"](), new _ships_js__WEBPACK_IMPORTED_MODULE_0__["Battleship"](), new _ships_js__WEBPACK_IMPORTED_MODULE_0__["Battleship"](), new _ships_js__WEBPACK_IMPORTED_MODULE_0__["Artillery"]()];
        //
        //for testings
        //this.ships = [new Raft()];

        // creating 2D array
        for (let i = 0; i < this.size; i++) {
            this.grids[i] = new Array(this.size);
            for (let j = 0; j < this.size; j++) {
                this.grids[i][j] = new Grid();
            }
            this.nameRows[i] = new Grid();
        }


        this.gameBoardElement = document.getElementById(gameBoardElement);
        this.nameRowsElement = document.getElementById(nameRowsElement);
        this.nameColsElement = document.getElementById(nameColsElement);

        // idMark marks grids locations for specific game board
        this.idMark = idMark;
        // if its possible to click on board
        this.active = false;
        // if all ships on board where destroyed(sunken)
        this.endGame = false;

        this.createGrids().randomLocations();

    }

    //
    // method creates game board by individual grids, that are placed in the HTML div element
    // and local 2d array grids
    createGrids() {
        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {

                // adding new grid to the HTML
                let newGrid = document.createElement('div');
                this.gameBoardElement.appendChild(newGrid);

                // assign location as ID to div element and into local grids array
                newGrid.id = this.idMark + j + i;
                this.grids[i][j].id = this.idMark + j + i;

                // callculate position for current new grid
                let topPosition = j * this.grids[i][j].size;
                let leftPosition = i * this.grids[i][j].size;

                // positioning new grid
                newGrid.style.top = (topPosition + newGrid.parentElement.offsetTop) + 'px';
                newGrid.style.left = (leftPosition + newGrid.parentElement.offsetLeft) + 'px';
            }
        }

        //this.createNameRows();
        // this.createNameCols();

        return this;
    };

    // creates elements for naming rows 'UNCOMPLETE!!'
    createNameRows() {
        console.log(this.nameRowsElement);

        let aplhabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

        for (let i = 0; i < this.size; i++) {
            let newGrid = document.createElement('div');
            this.nameRowsElement.appendChild(newGrid);

            let newText = document.createElement('p');
            newText.id = 'name-row-p';
            newText.textContent = aplhabet[i];


            newGrid.appendChild(newText);

            let topPosition = i * this.nameRows[i].size;

            newGrid.style.top = (topPosition + newGrid.parentElement.offsetTop) + 'px';
            //newGrid.style.left = newGrid.parentElement.offsetLeft + 'px';

        }
    }

    // not working correctly
    createNameCols() {
        console.log(this.nameColsElement);

        for (let i = 0; i < this.size; i++) {
            let newGrid = document.createElement('div');
            this.nameColsElement.appendChild(newGrid);

            let newText = document.createElement('p');
            newText.id = 'name-col-p';
            newText.textContent = i;


            newGrid.appendChild(newText);

            let leftPosition = i * this.nameRows[i].size;

            newGrid.style.top = 100 + 'px';
            //newGrid.style.left = newGrid.parentElement.offsetLeft + 'px';

        }

    }

    //
    // this method distribute ships across the board randomly
    // at first is randomly generated direction of the ship, according to that is called proper method
    randomLocations() {

        for (let i = 0; i < this.ships.length; i++) {

            // 0 = up, 1 = right direction of ship
            let direction = Math.floor(Math.random() * 2);

            switch (direction) {
                case 0:
                    this.randomLocationsBottom(this.ships[i], direction);
                    break;

                default:
                    this.randomLocationsRight(this.ships[i], direction);
                    break;
            }
        }

    };

    //
    // post ship at random positions in direction from TOP to BOTTOM
    randomLocationsBottom(ship, dir) {
        while (true) {
            let x = Math.floor(Math.random() * 10);
            let y = Math.floor(Math.random() * 10);

            // checking if the ship will fill inside the board;
            // if it does, the next is collision detection  to find out if grids in the direction are 'EMPTY';
            // in case that the ship doesn`t fill iside the board or there`s collision detected
            // the next iteration of the loop is started, and new coordinates are generated
            if ((x + ship.size) <= this.size) {

                if (this.checkCollision(x, y, ship.size, dir))
                    continue;
            }
            else
                continue;

            // changing state of grid on 'OCCUPIED' and storing coordinates inside the ship
            for (let j = 0; j < ship.size; j++) {
                this.grids[x + j][y].state = STATES.OCCUPIED;
                ship.locations.push(this.idMark + (x + j) + y);

                // showing new locations on board for testing
                //document.getElementById(this.idMark + (x + j) + y).style.backgroundColor = 'blue';
            }
            return;
        }
    };

    //
    // same as previous method, it`just in the direction from LEFT to RIGHT
    randomLocationsRight(ship, dir) {
        while (true) {
            let x = Math.floor(Math.random() * 10);
            let y = Math.floor(Math.random() * 10);

            if ((y + ship.size) <= this.size) {
                if (this.checkCollision(x, y, ship.size, dir))
                    continue;
            }
            else
                continue;

            for (let j = 0; j < ship.size; j++) {
                this.grids[x][y + j].state = STATES.OCCUPIED;
                ship.locations.push(this.idMark + x + (y + j));

                // showing new locations on board for testing
                //document.getElementById(this.idMark + x + (y + j)).style.backgroundColor = 'blue';
            }
            return;
        }
    };

    //
    //checikng collision, 
    //if the grids in the direction of the newly placed ship are OCCUPIED
    checkCollision(row, col, shipSize, dir) {
        switch (dir) {
            case 0:

                for (let i = row; i < (row + shipSize); i++) {
                    if (this.grids[i][col].state === STATES.OCCUPIED)
                        return true;
                }
                return false;

            default:
                for (let i = col; i < (col + shipSize); i++) {
                    if (this.grids[row][i].state === STATES.OCCUPIED)
                        return true;
                }
                return false;
        }
    };

    //
    // Event Handler for Left Mouse click on the grid
    // e.currentTarget is board div element, but we want grid, that is inside e.target
    fireOnMouseClick(e) {

        let result;
        if (e.target !== e.currentTarget) {

            result = this.fire(e.target.id);

            //alert('Clicked on row ' + row + ', col ' + col);
        }
        // this is to stop bubbling
        e.stopPropagation();
        return result;
    };

    //
    // this method contains whole logic of shooting on the grids
    fire(location) {

        // parssing 'row' and 'col' from string coordinates
        let row = location.substring(2, 3);
        let col = location.substring(3, 4);
        let target = document.getElementById(location);


        // if the target gird is 'EMPTY', player 'MISSED'
        // 'return values' are for AI, so it could determine if it hits
        // new target or previously hit target
        if (this.grids[row][col].state === STATES.EMPTY) {
            target.style.backgroundColor = '#bbb';
            this.grids[row][col].state = STATES.MISSED;

            return true;
            // else if grid is 'OCCUPIED', player hit the ship
        } else if (this.grids[row][col].state === STATES.OCCUPIED) {

            target.style.backgroundColor = 'red';
            this.grids[row][col].state = STATES.DESTROYED;

            // getting the currently hit ship, and increment number of hits
            let targetShip = this.ships.findIndex(f => f.locations.includes(location));
            this.ships[targetShip].hits++;

            // if number of hits is the same as size of the ship, the ship is sunken
            if (this.ships[targetShip].hits === this.ships[targetShip].size)
                this.ships[targetShip].sunken = true;

            // checking if game ended
            this.endGame = this.checkEndGame();

            return true;

            // checking if player fired on previously clicked grid
        } else if (this.grids[row][col].state === STATES.DESTROYED || this.grids[row][col].state === STATES.MISSED) {
            //alert('You already fired at this location.');
            return false;
        }
    };
    //
    // if all ships are sunked, game ends
    checkEndGame() {

        for (let i = 0; i < this.ships.length; i++) {
            if (!this.ships[i].sunken) {
                return false;
            }
        }
        return true;
    };
}

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Raft", function() { return Raft; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Scout", function() { return Scout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Battleship", function() { return Battleship; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Artillery", function() { return Artillery; });
//
// template for Ship,
class Ship {
    constructor() {
        this.name;
        this.size;
        this.locations = [];
        this.hits = 0;
        this.sunken = false;
    }
}

//
// Inheritance:
// Raft is type of ship whit its own "name" and "size"
class Raft extends Ship {
    constructor() {
        super();
        super.name = 'raft';
        super.size = 2;
    }
}

class Scout extends Ship {
    constructor() {
        super();
        super.name = 'scout';
        super.size = 3;
    }
}

class Battleship extends Ship {
    constructor() {
        super();
        super.name = 'battleship';
        super.size = 4;
    }
}

class Artillery extends Ship {
    constructor() {
        super();
        super.name = 'artillery';
        super.size = 5;
    }
}






/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Database; });
//
// this class contains functionality for working with IndexedDB
class Database {
    constructor() {
        this.version = 10; // increment if you need to call 'onupgradeneeded' funtion
    }

    // this method creates database if needed and destroy the previous one
    createDatabase() {
        return new Promise(resolve => {
            let request = window.indexedDB.open('players_db', this.version);

            request.onerror = function () {
                console.log('Database failed to open');
                resolve(false);
            };

            request.onsuccess = function (e) {

                console.log('Database opened successfully');
                resolve(true);
            }

            request.onupgradeneeded = function (e) {

                let db = request.result;


                // db.deleteObjectStore('players_db');

                db.createObjectStore('players_db', { keyPath: 'name' });

                console.log('Database setup complete');
                resolve(true);
            }



        });


    }

    // here you can add new players into database
    addData(data) {
        return new Promise(resolve => {

            let request = window.indexedDB.open('players_db', this.version);

            request.onerror = function () {
                console.log('Database failed to open');
                resolve(false);
            };

            request.onsuccess = function (e) {

                let db = request.result;

                data.forEach(player => {
                    db.transaction(['players_db'], 'readwrite').objectStore('players_db').add(player);
                });


                console.log('Database opened successfully');
                resolve(true);
            }


        });



    }

    // this method returns data of specific player
    getData(key) {

        return new Promise(resolve => {

            let request = window.indexedDB.open('players_db', this.version);

            request.onerror = function () {
                console.log('Database failed to open');
                resolve(false);
            };

            request.onsuccess = function (e) {

                let db = request.result;
                db.transaction('players_db').objectStore('players_db').get(key).onsuccess = function (e) {
                    console.log(e.target.result);
                    resolve(e.target.result);
                }

            }


        });


    }

    // updates data of currently playing players
    updateData(data) {

        return new Promise(resolve => {

            let request = window.indexedDB.open('players_db', this.version);

            request.onerror = function () {
                console.log('Database failed to open');
                resolve(false);
            };

            request.onsuccess = function (e) {

                let db = request.result;
                let objectStore = db.transaction(['players_db'], 'readwrite').objectStore('players_db');

                data.forEach(d => {
                    objectStore.put(d);
                });


                resolve(true);
            }
        });



    }

    // diplaying database as table on the screen
    async displayData() {

        return new Promise(resolve => {
            let request = window.indexedDB.open('players_db', this.version);

            request.onerror = function () {
                console.log('Database failed to open');
                alert('Database is Empty');

                resolve(false);
            };

            request.onsuccess = function (e) {
                let table = document.getElementById('player-data');
                while (table.firstChild) {
                    table.removeChild(table.lastChild);
                }


                let db = request.result;
                let objectStore = db.transaction('players_db').objectStore('players_db');
                objectStore.openCursor().onsuccess = function (e) {

                    let cursor = e.target.result;


                    if (cursor) {
                        let tr = document.createElement('tr');
                        let td = [];

                        for (let i = 0; i < 4; i++) {
                            td[i] = document.createElement('td');
                            tr.appendChild(td[i]);
                        }

                        td[0].textContent = cursor.value.name;
                        td[1].textContent = cursor.value.score;
                        td[2].textContent = cursor.value.games;
                        td[3].textContent = cursor.value.wins;

                        table.appendChild(tr);

                        cursor.continue();
                    }

                    document.getElementById('wrapper').hidden = true;
                    document.getElementById('tbl-db').hidden = false;
                }
                console.log('Database opened successfully');

                resolve(true);
            }
            console.log('displayed');

        });
    }



}

/***/ })
/******/ ]);